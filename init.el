;;; init.el --- The Emacs init file
;;
;;; Commentary:
;; This is the main configuration file for Emacs.
;; In this file:
;; - Set the bootstratp for straight.el
;; - Load use-package
;; - Load no-littering
;; - Set user-init-directory to ~/.cache/emacs
;; - Install the latest release of org
;; - Load the file ~/.emacs.d/myconfig.org
;;

;; Disable cl-functions warnings
(setq byte-compile-warnings '(cl-functions))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set up straight package manager
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
        "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
        'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helper functions

;; Helper function for loading elisp files
(defun load-elisp-if-exists (f)
  "load the elisp file only if it exists and is readable"
  (if (and (file-exists-p f)
           (file-readable-p f))
      (load-file f)))
;; Helper function for loading org-mode files
(defun load-orgfile-if-exists (f)
  "load the elisp file only if it exists and is readable"
  (if (and (file-exists-p f)
           (file-readable-p f))
      (org-babel-load-file f)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Keep emacs clean!
;; Change the user-emacs-directory to keep unwanted things out of ~/.emacs.d
(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory))
;; Use no-littering to automatically set common paths to the new user-emacs-directory
(use-package no-littering)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This is a patch for using emacs with fake home directories
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load-elisp-if-exists custom-file)   ;; custom-file not loaded with fancy home variables

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Install org-mode
(straight-use-package 'org)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load myconfig file.
(let ((config-el (expand-file-name "~/.emacs.d/myconfig.el"))
      (config-org (expand-file-name "~/.emacs.d/myconfig.org")))
  (if (and (file-exists-p config-el)
           (time-less-p (file-attribute-modification-time (file-attributes config-org))
                        (file-attribute-modification-time (file-attributes config-el))))
      (load-file config-el)
    (org-babel-load-file config-org)))

(provide 'init)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init.el ends here
